package com.epam.clients.rest.clients;

import com.epam.clients.rest.Config;
import com.epam.clients.rest.util.TokenReceiving;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import org.apache.http.HttpEntity;
import org.apache.http.HttpHeaders;
import org.apache.http.HttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.util.EntityUtils;

import java.io.IOException;

public class AdminServiceClient {

    private static final String SERVER_ERROR = "Server Error";

    private static String token;
    private static String uriPattern;
    private static CloseableHttpClient httpClient = HttpClients.createDefault();

    static {
        try {
            Config config = Config.getInstance();
            uriPattern = "http://" +
                    config.getIp() +
                    ":" +
                    config.getPort() +
                    "/userlib/library/admin/";
            token = new TokenReceiving("Egorka99", "123").getUserToken();
        } catch (IOException e) {
            System.out.println("Ошибка: " + e.getMessage());
        }
    }

    public static String registerNewUser(String firstName, String lastName, String nickname, String password, int roleId) throws IOException {

        String uriBuilder = uriPattern +
                "register?firstname=" +
                firstName +
                "&lastname=" +
                lastName +
                "&nickname=" +
                nickname +
                "&password=" +
                password +
                "&roleId=" +
                roleId;
        HttpPost httpPost = new HttpPost(uriBuilder);

        httpPost.setHeader(HttpHeaders.CONTENT_TYPE, "application/json");
        httpPost.setHeader(HttpHeaders.AUTHORIZATION, "Bearer " + token);

        HttpResponse httpResponse = httpClient.execute(httpPost);

        HttpEntity entity = httpResponse.getEntity();

        return entity != null ? EntityUtils.toString(entity) : SERVER_ERROR;
    }

    public static String blockUser(int userId) throws IOException {

        String uriBuilder = uriPattern +
                "block/?userId=" +
                userId;
        HttpPost httpPost = new HttpPost(uriBuilder);

        httpPost.setHeader(HttpHeaders.CONTENT_TYPE, "application/json");
        httpPost.setHeader(HttpHeaders.AUTHORIZATION, "Bearer " + token);

        HttpResponse httpResponse = httpClient.execute(httpPost);

        HttpEntity entity = httpResponse.getEntity();

        return entity != null ? EntityUtils.toString(entity) : SERVER_ERROR;
    }

    public static String getUserHistory() throws IOException {

        GsonBuilder builder = new GsonBuilder().setDateFormat("mm.dd.yyyy");
        Gson gson = builder.create();

        String uriBuilder = uriPattern +
                "history";
        HttpGet httpGet = new HttpGet(uriBuilder);

        httpGet.setHeader(HttpHeaders.CONTENT_TYPE, "application/json");
        httpGet.setHeader(HttpHeaders.AUTHORIZATION, "Bearer " + token);

        HttpResponse httpResponse = httpClient.execute(httpGet);

        HttpEntity entity = httpResponse.getEntity();

        return entity != null ? EntityUtils.toString(entity) : SERVER_ERROR;
    }

    public static void main(String[] args) throws IOException {
        //http://25.134.97.50:8888/userlib/library/admin/register?firstname=newg&lastname=userc&nickname=user1234&password=123&roleId=1
        System.out.println(registerNewUser("new", "user", "user12345", "123", 1));
        //http://25.134.97.50:8888/userlib/library/admin/block/?userId=2
        System.out.println(blockUser(0));
        //http://25.134.97.50:8888/userlib/library/admin/history
        System.out.println(getUserHistory());
    }
}
