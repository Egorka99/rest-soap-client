package com.epam.clients.rest.clients;

import com.epam.clients.rest.Config;
import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.util.EntityUtils;

import java.io.IOException;

public class SearchBookServiceClient {

    private static final String SERVER_ERROR = "Server Error";

    private static CloseableHttpClient httpClient = HttpClients.createDefault();

    private static String uriPattern;

    static {
        try {
            Config config = Config.getInstance();
            uriPattern = "http://" +
                    config.getIp() +
                    ":" +
                    config.getPort() +
                    "/userlib/library/search/";
        } catch (IOException e) {
            System.out.println("Ошибка: " + e.getMessage());
        }
    }

    public static String searchByAuthorName(String authorName) throws IOException {

        String uriBuilder = uriPattern +
                "findbyauthorname?authorName=" +
                authorName;
        HttpGet httpGet = new HttpGet(uriBuilder);

        HttpResponse httpResponse = httpClient.execute(httpGet);

        HttpEntity entity = httpResponse.getEntity();

        return entity != null ? EntityUtils.toString(entity) : SERVER_ERROR;
    }

    public static String searchByBookName(String bookName) throws IOException {

        String uriBuilder = uriPattern +
                "findbybookname?bookName=" +
                bookName;
        HttpGet httpGet = new HttpGet(uriBuilder);

        HttpResponse httpResponse = httpClient.execute(httpGet);

        HttpEntity entity = httpResponse.getEntity();

        return entity != null ? EntityUtils.toString(entity) : SERVER_ERROR;
    }
    public static String searchByIsbn(String isbn) throws IOException {

        String uriBuilder = uriPattern +
                "findbyisbn?bookisbn=" +
                isbn;
        HttpGet httpGet = new HttpGet(uriBuilder);

        HttpResponse httpResponse = httpClient.execute(httpGet);

        HttpEntity entity = httpResponse.getEntity();

        return entity != null ? EntityUtils.toString(entity) : SERVER_ERROR;
    }
    public static String searchByUsersBookmark(int userId) throws IOException {

        String uriBuilder = uriPattern +
                "findusersbookmark?userId=" +
                userId;
        HttpGet httpGet = new HttpGet(uriBuilder);

        HttpResponse httpResponse = httpClient.execute(httpGet);

        HttpEntity entity = httpResponse.getEntity();

        return entity != null ? EntityUtils.toString(entity) : SERVER_ERROR;
    }
    public static String searchByYearPageName(int year, int pageCount, String name) throws IOException {

        String uriBuilder = uriPattern +
                "findbyyearpagename?year=" +
                year +
                "&page=" +
                pageCount +
                "&bookname=" +
                name;
        HttpGet httpGet = new HttpGet(uriBuilder);

        HttpResponse httpResponse = httpClient.execute(httpGet);

        HttpEntity entity = httpResponse.getEntity();

        return entity != null ? EntityUtils.toString(entity) : SERVER_ERROR;
    }
    public static String searchByYearsRange(int startYear, int endYear) throws IOException {

        String uriBuilder = uriPattern +
                "findbyyearsrange?fromyear=" +
                startYear +
                "&toyear=" +
                endYear;
        HttpGet httpGet = new HttpGet(uriBuilder);

        HttpResponse httpResponse = httpClient.execute(httpGet);

        HttpEntity entity = httpResponse.getEntity();

        return entity != null ? EntityUtils.toString(entity) : SERVER_ERROR;
    }

    public static void main(String[] args) throws IOException {
        //http://25.134.97.50:8888/userlib/library/search/findbybookname?bookName=q
        System.out.println(searchByBookName("q"));
        //http://25.134.97.50:8888/userlib/library/search/findbyauthorname?authorName=С
        System.out.println(searchByAuthorName("C"));
        //http://25.134.97.50:8888/userlib/library/search/findbyisbn?bookisbn=978-0452261342
        System.out.println(searchByIsbn("978-0452261342"));
        //http://25.134.97.50:8888/userlib/library/search/findusersbookmark?userId=2
        System.out.println(searchByUsersBookmark(2));
        //http://25.134.97.50:8888/userlib/library/search/findbyyearpagename?year=2012&page=22&bookname=wqe
        System.out.println(searchByYearPageName(2012,22,"wqe"));
        //http://25.134.97.50:8888/userlib/library/search/findbyyearsrange?fromyear=1&toyear=2010
        System.out.println(searchByYearsRange(1,2010));
    }
}
