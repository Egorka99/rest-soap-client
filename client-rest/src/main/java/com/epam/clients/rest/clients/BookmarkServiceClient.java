package com.epam.clients.rest.clients;

import com.epam.clients.rest.Config;
import com.epam.clients.rest.util.TokenReceiving;
import org.apache.http.HttpEntity;
import org.apache.http.HttpHeaders;
import org.apache.http.HttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.util.EntityUtils;

import java.io.IOException;

public class BookmarkServiceClient {

    private static final String SERVER_ERROR = "Server Error";

    private static CloseableHttpClient httpClient = HttpClients.createDefault();

    private static String token;
    private static String uriPattern;

    static {
        try {
            Config config = Config.getInstance();
            uriPattern = "http://" +
                    config.getIp() +
                    ":" +
                    config.getPort() +
                    "/userlib/library/bookmark/";
            token = new TokenReceiving("1", "1").getUserToken();
        } catch (IOException e) {
            System.out.println("Ошибка: " + e.getMessage());
        }
    }

    public static String getBookmarks(int userId) throws IOException {

        String uriBuilder = uriPattern +
                "?userId=" +
                userId;
        HttpGet httpGet = new HttpGet(uriBuilder);

        HttpResponse httpResponse = httpClient.execute(httpGet);

        HttpEntity entity = httpResponse.getEntity();

        return entity != null ? EntityUtils.toString(entity) : SERVER_ERROR;
    }

    public static String addBookmark(int userId, int bookId, int pageNumber) throws IOException {

        String uriBuilder = uriPattern +
                "/addbookmark?userId=" +
                userId +
                "&bookId=" +
                bookId +
                "&pageNumber=" +
                pageNumber;
        HttpPost httpPost = new HttpPost(uriBuilder);

        httpPost.setHeader(HttpHeaders.CONTENT_TYPE, "application/json");
        httpPost.setHeader(HttpHeaders.AUTHORIZATION, "Bearer " + token);

        HttpResponse httpResponse = httpClient.execute(httpPost);

        HttpEntity entity = httpResponse.getEntity();

        return entity != null ? EntityUtils.toString(entity) : SERVER_ERROR;
    }

    public static String deleteBookmark(int bookmarkId) throws IOException {

        String uriBuilder = uriPattern +
                "deletebookmark?bookmarkId=" +
                bookmarkId;
        HttpPost httpPost = new HttpPost(uriBuilder);

        httpPost.setHeader(HttpHeaders.CONTENT_TYPE, "application/json");
        httpPost.setHeader(HttpHeaders.AUTHORIZATION, "Bearer " + token);

        HttpResponse httpResponse = httpClient.execute(httpPost);

        HttpEntity entity = httpResponse.getEntity();

        return entity != null ? EntityUtils.toString(entity) : SERVER_ERROR;
    }

    public static void main(String[] args) throws IOException {
        //http://25.134.97.50:8888/userlib/library/bookmark?userId=2
        System.out.println(getBookmarks(2));
        //http://25.134.97.50:8888/userlib/library/bookmark/addbookmark?userId=2&bookId=15&pageNumber=12
        System.out.println(addBookmark(2, 15, 1));
        //http://25.134.97.50:8888/userlib/library/bookmark/deletebookmark/?bookmarkId=15
        System.out.println(deleteBookmark(15));
    }
}
