package com.epam.clients.rest.util;

import com.epam.clients.rest.Config;
import org.apache.http.HttpResponse;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;

import java.io.IOException;

public class TokenReceiving {

    private static CloseableHttpClient httpClient = HttpClients.createDefault();

    private String login;
    private String password;
    private Config config;

    public TokenReceiving(String login, String password) throws IOException {
        config = Config.getInstance();
        this.login = login;
        this.password = password;
    }

    public String getUserToken() throws IOException {

        StringBuilder uriBuilder = new StringBuilder();
        uriBuilder.append("http://");
        uriBuilder.append(config.getIp());
        uriBuilder.append(":");
        uriBuilder.append(config.getPort());
        uriBuilder.append("/userlib/signin?nickname=");
        uriBuilder.append(login);
        uriBuilder.append("&password=");
        uriBuilder.append(password);

        HttpPost httpPost = new HttpPost(uriBuilder.toString());

        HttpResponse httpResponse = httpClient.execute(httpPost);

        String authorizationHeader = httpResponse.getFirstHeader("Authorization").getValue();

        return authorizationHeader.substring("Bearer".length()).trim();

    }
}
