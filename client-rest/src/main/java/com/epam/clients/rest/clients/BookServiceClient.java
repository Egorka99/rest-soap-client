package com.epam.clients.rest.clients;

import com.epam.clients.rest.Config;
import com.epam.clients.rest.util.TokenReceiving;
import org.apache.http.HttpEntity;
import org.apache.http.HttpHeaders;
import org.apache.http.HttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.util.EntityUtils;

import java.io.IOException;

public class BookServiceClient {

    private static final String SERVER_ERROR = "Server Error";

    private static String token;
    private static String uriPattern;
    private static CloseableHttpClient httpClient = HttpClients.createDefault();

    static {
        try {
            Config config = Config.getInstance();
            uriPattern = "http://" +
                    config.getIp() +
                    ":" +
                    config.getPort() +
                    "/userlib/library/";
            token = new TokenReceiving("1", "1").getUserToken();
        } catch (IOException e) {
            System.out.println("Ошибка: " + e.getMessage());
        }
    }

    public static String addBook(String name, int releaseYear, int pageCount, String publisher, String bookISBN, int authorId) throws IOException {

        String uriBuilder = uriPattern +
                "addbook?bookName=" +
                name +
                "&releaseYear=" +
                releaseYear +
                "&pageCount=" +
                pageCount +
                "&publisher=" +
                publisher +
                "&bookISBN=" +
                bookISBN +
                "&authorId=" +
                authorId;
        HttpPost httpPost = new HttpPost(uriBuilder);

        httpPost.setHeader(HttpHeaders.CONTENT_TYPE, "application/json");
        httpPost.setHeader(HttpHeaders.AUTHORIZATION, "Bearer " + token);

        HttpResponse httpResponse = httpClient.execute(httpPost);

        HttpEntity entity = httpResponse.getEntity();

        return entity != null ? EntityUtils.toString(entity) : SERVER_ERROR;
    }

    public static String deleteBook(int bookId) throws IOException {

        String uriBuilder = uriPattern +
                "deletebook?bookId=" +
                bookId;
        HttpPost httpPost = new HttpPost(uriBuilder);

        httpPost.setHeader(HttpHeaders.CONTENT_TYPE, "application/json");
        httpPost.setHeader(HttpHeaders.AUTHORIZATION, "Bearer " + token);

        HttpResponse httpResponse = httpClient.execute(httpPost);

        HttpEntity entity = httpResponse.getEntity();

        return entity != null ? EntityUtils.toString(entity) : SERVER_ERROR;
    }

    public static String getAllBooks() throws IOException {
        HttpGet httpGet = new HttpGet(uriPattern);

        HttpResponse httpResponse = httpClient.execute(httpGet);

        HttpEntity entity = httpResponse.getEntity();

        return entity != null ? EntityUtils.toString(entity) : SERVER_ERROR;
    }

    public static void main(String[] args) throws IOException {
        //http://25.134.97.50:8888/userlib/library/addbook?bookName=kakkakat&releaseYear=2020&pageCount=12808&publisher=Tvoyamama&authorId=2&bookISBN=923-3451237562
        System.out.println(addBook("name", 2012, 123, "publisher", "000-1236731233", 2));
        //http://25.134.97.50:8888/userlib/library/deletebook?bookId=30
        System.out.println(deleteBook(30));
        //http://25.134.97.50:8888/userlib/library/getAllBooks();
        System.out.println(getAllBooks());
    }
}
