package com.epam.clients.rest.clients;

import com.epam.clients.rest.Config;
import com.epam.clients.rest.util.TokenReceiving;
import org.apache.http.HttpEntity;
import org.apache.http.HttpHeaders;
import org.apache.http.HttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.util.EntityUtils;

import java.io.IOException;

public class AuthorServiceClient {

    private static final String SERVER_ERROR = "Server Error";

    private static String token;
    private static String uriPattern;
    private static CloseableHttpClient httpClient = HttpClients.createDefault();

    static {
        try {
            Config config = Config.getInstance();
            uriPattern = "http://" +
                    config.getIp() +
                    ":" +
                    config.getPort() +
                    "/userlib/library/author/";
            token = new TokenReceiving("1", "1").getUserToken();
        } catch (IOException e) {
            System.out.println("Ошибка: " + e.getMessage());
        }
    }

    public static String getAllAuthors() throws IOException {

        HttpGet httpGet = new HttpGet(uriPattern);

        HttpResponse httpResponse = httpClient.execute(httpGet);

        HttpEntity entity = httpResponse.getEntity();

        return entity != null ? EntityUtils.toString(entity) : SERVER_ERROR;
    }

    public static String addAuthor(String authorName, String secondName, String lastName, String dob) throws IOException {

        String uriBuilder = uriPattern +
                "/addauthor?authorName=" +
                authorName +
                "&secondName=" +
                secondName +
                "&lastName=" +
                lastName +
                "&dob=" +
                dob;
        HttpPost httpPost = new HttpPost(uriBuilder);

        httpPost.setHeader(HttpHeaders.CONTENT_TYPE, "application/json");
        httpPost.setHeader(HttpHeaders.AUTHORIZATION, "Bearer " + token);

        HttpResponse httpResponse = httpClient.execute(httpPost);

        HttpEntity entity = httpResponse.getEntity();

        return entity != null ? EntityUtils.toString(entity) : SERVER_ERROR;
    }

    public static String deleteAuthor(int authorId) throws IOException {

        String uriBuilder = uriPattern +
                "deleteauthor?authorId=" +
                authorId;
        HttpPost httpPost = new HttpPost(uriBuilder);

        httpPost.setHeader(HttpHeaders.CONTENT_TYPE, "application/json");
        httpPost.setHeader(HttpHeaders.AUTHORIZATION, "Bearer " + token);

        HttpResponse httpResponse = httpClient.execute(httpPost);

        HttpEntity entity = httpResponse.getEntity();

        return entity != null ? EntityUtils.toString(entity) : SERVER_ERROR;
    }

    public static void main(String[] args) throws IOException {
        //"http://25.134.97.50:8888/userlib/library/author/
        System.out.println(getAllAuthors());
        //http://25.134.97.50:8888/userlib/library/author/addauthor?authorName=Roma&secondName=Vyacheslavovic&lastName=Ivanov&dob=1997-04-11
        System.out.println(addAuthor("Roma", "Vyacheslavovic", "Ivanov", "1997-04-11"));
        //http://25.134.97.50:8888/userlib/library/author/deleteauthor?authorId=34
        System.out.println(deleteAuthor(36));
    }
}
