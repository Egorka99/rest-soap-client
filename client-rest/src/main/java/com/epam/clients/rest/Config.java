package com.epam.clients.rest;

import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

public class Config {

    private static final String CONFIG_FILE_PATH = "application.properties";
    private static final String IP_PROPERTY = "ip_address";
    private static final String PORT_PROPERTY = "port";

    private static Config instance;
    private String ip;
    private String port;


    public static Config getInstance() throws IOException {
        if (instance == null) {
            instance = new Config();
        }
        return instance;
    }

    private Config() throws IOException {
        InputStream is = getClass().getClassLoader().getResourceAsStream(CONFIG_FILE_PATH);

        Properties properties = new Properties();
        properties.load(is);

        ip = properties.getProperty(IP_PROPERTY);
        port = properties.getProperty(PORT_PROPERTY);
    }

    public String getIp() {
        return ip;
    }

    public String getPort() {
        return port;
    }


}
