package com.epam.clients.soap;

import com.epam.clients.soap.wsimport.DataException;
import com.epam.clients.soap.wsimport.SearchService;

import javax.xml.namespace.QName;
import javax.xml.ws.Service;
import java.net.MalformedURLException;
import java.net.URL;

public class SearchBookWebServiceClient {
    public static void main(String[] args) throws MalformedURLException {
        URL url = new URL("http://25.134.97.50:8888/userlib/library/searchBooks?wsdl");
        QName qname = new QName("http://soap.epam.com/", "SearchServiceImplService");

        Service service = Service.create(url, qname);

        SearchService searchService = service.getPort(SearchService.class);

        try {
            searchService.findByBookName("q").getItem().forEach(System.out::println);
            System.out.println("---");
            searchService.findByAuthorName("Auth").getItem().forEach(System.out::println);
            System.out.println("---");
            searchService.findByYearPageName(1997,434,"Лаб").getItem().forEach(System.out::println);
            System.out.println("---");
            searchService.findByYearsRange(2012,2012).getItem().forEach(System.out::println);
            System.out.println("---");
            System.out.println(searchService.findByiISBN("000-0000000002").getBookName());
            System.out.println("---");
            searchService.findUsersBookmark(2).getItem().forEach(System.out::println);
        } catch (DataException e) {
            System.out.println(e.getMessage());
        }

    }

}
