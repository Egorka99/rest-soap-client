package com.epam.clients.soap;

import com.epam.clients.soap.util.AuthorizationUtil;
import com.epam.clients.soap.wsimport.BookService;
import com.epam.clients.soap.wsimport.DataException;

import javax.xml.namespace.QName;
import javax.xml.ws.Service;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.Scanner;

public class BookWebServiceClient {
    public static void main(String[] args) throws MalformedURLException {
        URL url = new URL("http://25.134.97.50:8888/userlib/library/books?wsdl");
        QName qname = new QName("http://soap.epam.com/", "BookServiceImplService");

        Service service = Service.create(url, qname);

        BookService bookService = service.getPort(BookService.class);

        Scanner scanner = new Scanner(System.in);
        System.out.println("Вход в систему");
        System.out.println("Никнейм:");
        String nickname = scanner.nextLine();
        System.out.println("Пароль:");
        String password = scanner.nextLine();

        AuthorizationUtil.putDataInRequestHeader(service, bookService, nickname, password);

        //bookService.addBook("some book", 2020, 123, "000-0003000011", "publ", 6);
        //bookService.deleteBook(31);
        //bookService.getAll().getItem().forEach(System.out::println);
    }
}
