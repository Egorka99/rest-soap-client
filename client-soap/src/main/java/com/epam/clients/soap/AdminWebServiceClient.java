package com.epam.clients.soap;

import com.epam.clients.soap.util.AuthorizationUtil;
import com.epam.clients.soap.wsimport.AdminService;
import com.epam.clients.soap.wsimport.UserException;

import javax.xml.namespace.QName;
import javax.xml.ws.Service;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.Scanner;

public class AdminWebServiceClient {

    public static void main(String[] args) throws MalformedURLException, UserException {

        URL url = new URL("http://25.134.97.50:8888/userlib/library/admin?wsdl");
        QName qname = new QName("http://soap.epam.com/", "AdminServiceImplService");

        Service service = Service.create(url, qname);

        AdminService adminService = service.getPort(AdminService.class);

        Scanner scanner = new Scanner(System.in);
        System.out.println("Вход в систему");
        System.out.println("Никнейм:");
        String nickname = scanner.nextLine();
        System.out.println("Пароль:");
        String password = scanner.nextLine();

        AuthorizationUtil.putDataInRequestHeader(service, adminService, nickname, password);


        adminService.blockUser(0);
        //adminService.register("2", "2", "222", "33", 2);
        // adminService.getUserLogs().getItem().forEach(System.out::println);
    }
}
