
package com.epam.clients.soap.wsimport;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlElementDecl;
import javax.xml.bind.annotation.XmlRegistry;
import javax.xml.namespace.QName;


/**
 * This object contains factory methods for each 
 * Java content interface and Java element interface 
 * generated in the com.epam.soap.interfaces package. 
 * <p>An ObjectFactory allows you to programatically 
 * construct new instances of the Java representation 
 * for XML content. The Java representation of XML 
 * content can consist of schema derived interfaces 
 * and classes representing the binding of schema 
 * type definitions, element declarations and model 
 * groups.  Factory methods for each of these are 
 * provided in this class.
 * 
 */
@XmlRegistry
public class ObjectFactory {

    private final static QName _DataException_QNAME = new QName("http://interfaces.soap.epam.com/", "DataException");
    private final static QName _Log_QNAME = new QName("http://interfaces.soap.epam.com/", "log");

    /**
     * Create a new ObjectFactory that can be used to create new instances of schema derived classes for package: com.epam.soap.interfaces
     * 
     */
    public ObjectFactory() {
    }

    /**
     * Create an instance of {@link DataException }
     * 
     */
    public DataException createDataException() {
        return new DataException();
    }

    /**
     * Create an instance of {@link Log }
     *
     */
    public Log createLog() {
        return new Log();
    }

    /**
     * Create an instance of {@link ResponseWrapper }
     * 
     */
    public ResponseWrapper createResponseWrapper() {
        return new ResponseWrapper();
    }

    /**
     * Create an instance of {@link LogArray }
     * 
     */
    public LogArray createLogArray() {
        return new LogArray();
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link DataException }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://interfaces.soap.epam.com/", name = "DataException")
    public JAXBElement<DataException> createDataException(DataException value) {
        return new JAXBElement<DataException>(_DataException_QNAME, DataException.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Log }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://interfaces.soap.epam.com/", name = "log")
    public JAXBElement<Log> createLog(Log value) {
        return new JAXBElement<Log>(_Log_QNAME, Log.class, null, value);
    }

}
