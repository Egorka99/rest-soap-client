package com.epam.clients.soap;

import com.epam.clients.soap.util.AuthorizationUtil;
import com.epam.clients.soap.wsimport.BookmarkService;
import com.epam.clients.soap.wsimport.DataException;

import javax.xml.namespace.QName;
import javax.xml.ws.Service;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.Scanner;


public class BookmarkWebServiceClient {
    public static void main(String[] args) throws MalformedURLException, DataException {
        URL url = new URL("http://25.134.97.50:8888/userlib/library/bookmarks?wsdl");
        QName qname = new QName("http://soap.epam.com/", "BookmarkServiceImplService");

        Service service = Service.create(url, qname);

        BookmarkService bookmarkService = service.getPort(BookmarkService.class);

        Scanner scanner = new Scanner(System.in);
        System.out.println("Вход в систему");
        System.out.println("Никнейм:");
        String nickname = scanner.nextLine();
        System.out.println("Пароль:");
        String password = scanner.nextLine();

        AuthorizationUtil.putDataInRequestHeader(service, bookmarkService, nickname, password);

        // bookmarkService.addBookmark(2,15,20);
//        bookmarkService.deleteBookmark(15);
//        bookmarkService.getBookmarks(2).getItem().forEach(System.out::println);
        System.out.println("Успешно!");

    }

}
