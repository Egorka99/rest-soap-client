
package com.epam.clients.soap.wsimport;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for log complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="log">
 *   &lt;complexContent>
 *     &lt;extension base="{http://interfaces.soap.epam.com/}entity">
 *       &lt;sequence>
 *       &lt;/sequence>
 *       &lt;attribute name="logId" use="required" type="{http://www.w3.org/2001/XMLSchema}long" />
 *       &lt;attribute name="userId" use="required" type="{http://www.w3.org/2001/XMLSchema}long" />
 *       &lt;attribute name="text" type="{http://www.w3.org/2001/XMLSchema}string" />
 *     &lt;/extension>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "log")
public class Log
    extends Entity
{

    @XmlAttribute(name = "logId", required = true)
    protected long logId;
    @XmlAttribute(name = "userId", required = true)
    protected long userId;
    @XmlAttribute(name = "text")
    protected String text;

    /**
     * Gets the value of the logId property.
     * 
     */
    public long getLogId() {
        return logId;
    }

    /**
     * Sets the value of the logId property.
     * 
     */
    public void setLogId(long value) {
        this.logId = value;
    }

    /**
     * Gets the value of the userId property.
     * 
     */
    public long getUserId() {
        return userId;
    }

    /**
     * Sets the value of the userId property.
     * 
     */
    public void setUserId(long value) {
        this.userId = value;
    }

    /**
     * Gets the value of the text property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getText() {
        return text;
    }

    /**
     * Sets the value of the text property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setText(String value) {
        this.text = value;
    }

    @Override
    public String toString() {
        return "Log{" +
                "logId=" + logId +
                ", userId=" + userId +
                ", text='" + text + '\'' +
                '}';
    }
}
