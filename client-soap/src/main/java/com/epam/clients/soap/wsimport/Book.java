
package com.epam.clients.soap.wsimport;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for book complex type.
 *
 * <p>The following schema fragment specifies the expected content contained within this class.
 *
 * <pre>
 * &lt;complexType name="book">
 *   &lt;complexContent>
 *     &lt;extension base="{http://interfaces.soap.epam.com/}entity">
 *       &lt;sequence>
 *         &lt;element name="author" type="{http://interfaces.soap.epam.com/}author" minOccurs="0"/>
 *       &lt;/sequence>
 *       &lt;attribute name="bookId" use="required" type="{http://www.w3.org/2001/XMLSchema}long" />
 *       &lt;attribute name="bookName" type="{http://www.w3.org/2001/XMLSchema}string" />
 *       &lt;attribute name="releaseYear" use="required" type="{http://www.w3.org/2001/XMLSchema}int" />
 *       &lt;attribute name="pageCount" use="required" type="{http://www.w3.org/2001/XMLSchema}int" />
 *       &lt;attribute name="isbn" type="{http://www.w3.org/2001/XMLSchema}string" />
 *       &lt;attribute name="publisher" type="{http://www.w3.org/2001/XMLSchema}string" />
 *     &lt;/extension>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "book", propOrder = {
        "author"
})
public class Book
        extends Entity {

    protected Author author;
    @XmlAttribute(name = "bookId", required = true)
    protected long bookId;
    @XmlAttribute(name = "bookName")
    protected String bookName;
    @XmlAttribute(name = "releaseYear", required = true)
    protected int releaseYear;
    @XmlAttribute(name = "pageCount", required = true)
    protected int pageCount;
    @XmlAttribute(name = "isbn")
    protected String isbn;
    @XmlAttribute(name = "publisher")
    protected String publisher;

    /**
     * Gets the value of the author property.
     *
     * @return possible object is
     * {@link Author }
     */
    public Author getAuthor() {
        return author;
    }

    /**
     * Sets the value of the author property.
     *
     * @param value allowed object is
     *              {@link Author }
     */
    public void setAuthor(Author value) {
        this.author = value;
    }

    /**
     * Gets the value of the bookId property.
     */
    public long getBookId() {
        return bookId;
    }

    /**
     * Sets the value of the bookId property.
     */
    public void setBookId(long value) {
        this.bookId = value;
    }

    /**
     * Gets the value of the bookName property.
     *
     * @return possible object is
     * {@link String }
     */
    public String getBookName() {
        return bookName;
    }

    /**
     * Sets the value of the bookName property.
     *
     * @param value allowed object is
     *              {@link String }
     */
    public void setBookName(String value) {
        this.bookName = value;
    }

    /**
     * Gets the value of the releaseYear property.
     */
    public int getReleaseYear() {
        return releaseYear;
    }

    /**
     * Sets the value of the releaseYear property.
     */
    public void setReleaseYear(int value) {
        this.releaseYear = value;
    }

    /**
     * Gets the value of the pageCount property.
     */
    public int getPageCount() {
        return pageCount;
    }

    /**
     * Sets the value of the pageCount property.
     */
    public void setPageCount(int value) {
        this.pageCount = value;
    }

    /**
     * Gets the value of the isbn property.
     *
     * @return possible object is
     * {@link String }
     */
    public String getIsbn() {
        return isbn;
    }

    /**
     * Sets the value of the isbn property.
     *
     * @param value allowed object is
     *              {@link String }
     */
    public void setIsbn(String value) {
        this.isbn = value;
    }

    /**
     * Gets the value of the publisher property.
     *
     * @return possible object is
     * {@link String }
     */
    public String getPublisher() {
        return publisher;
    }

    /**
     * Sets the value of the publisher property.
     *
     * @param value allowed object is
     *              {@link String }
     */
    public void setPublisher(String value) {
        this.publisher = value;
    }

    @Override
    public String toString() {
        return "Book{" +
                "author=" + author +
                ", bookId=" + bookId +
                ", bookName='" + bookName + '\'' +
                ", releaseYear=" + releaseYear +
                ", pageCount=" + pageCount +
                ", isbn='" + isbn + '\'' +
                ", publisher='" + publisher + '\'' +
                '}';
    }
}
