package com.epam.clients.soap.wsimport;

import javax.xml.bind.annotation.*;
import javax.xml.datatype.XMLGregorianCalendar;

@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "author")
public class Author
        extends Entity {

    @XmlAttribute(name = "authorId", required = true)
    protected long authorId;
    @XmlAttribute(name = "name")
    protected String name;
    @XmlAttribute(name = "secondName")
    protected String secondName;
    @XmlAttribute(name = "lastName")
    protected String lastName;
    @XmlAttribute(name = "dob")
    @XmlSchemaType(name = "dateTime")
    protected XMLGregorianCalendar dob;

    /**
     * Gets the value of the authorId property.
     */
    public long getAuthorId() {
        return authorId;
    }

    /**
     * Sets the value of the authorId property.
     */
    public void setAuthorId(long value) {
        this.authorId = value;
    }

    /**
     * Gets the value of the name property.
     *
     * @return possible object is
     * {@link String }
     */
    public String getName() {
        return name;
    }

    /**
     * Sets the value of the name property.
     *
     * @param value allowed object is
     *              {@link String }
     */
    public void setName(String value) {
        this.name = value;
    }

    /**
     * Gets the value of the secondName property.
     *
     * @return possible object is
     * {@link String }
     */
    public String getSecondName() {
        return secondName;
    }

    /**
     * Sets the value of the secondName property.
     *
     * @param value allowed object is
     *              {@link String }
     */
    public void setSecondName(String value) {
        this.secondName = value;
    }

    /**
     * Gets the value of the lastName property.
     *
     * @return possible object is
     * {@link String }
     */
    public String getLastName() {
        return lastName;
    }

    /**
     * Sets the value of the lastName property.
     *
     * @param value allowed object is
     *              {@link String }
     */
    public void setLastName(String value) {
        this.lastName = value;
    }

    /**
     * Gets the value of the dob property.
     *
     * @return possible object is
     * {@link XMLGregorianCalendar }
     */
    public XMLGregorianCalendar getDob() {
        return dob;
    }

    /**
     * Sets the value of the dob property.
     *
     * @param value allowed object is
     *              {@link XMLGregorianCalendar }
     */
    public void setDob(XMLGregorianCalendar value) {
        this.dob = value;
    }

    @Override
    public String toString() {
        return "Author{" +
                "authorId=" + authorId +
                ", name='" + name + '\'' +
                ", secondName='" + secondName + '\'' +
                ", lastName='" + lastName + '\'' +
                ", dob=" + dob +
                '}';
    }
}