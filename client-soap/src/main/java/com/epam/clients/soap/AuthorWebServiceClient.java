package com.epam.clients.soap;

import com.epam.clients.soap.util.AuthorizationUtil;
import com.epam.clients.soap.wsimport.AuthorService;
import com.epam.clients.soap.wsimport.DataException;

import javax.xml.namespace.QName;
import javax.xml.ws.Service;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.Scanner;

public class AuthorWebServiceClient {
    public static void main(String[] args) throws MalformedURLException, DataException {
        URL url = new URL("http://25.134.97.50:8888/userlib/library/authors?wsdl");
        QName qname = new QName("http://soap.epam.com/", "AuthorServiceImplService");

        Service service = Service.create(url, qname);

        AuthorService authorService = service.getPort(AuthorService.class);

        Scanner scanner = new Scanner(System.in);
        System.out.println("Вход в систему");
        System.out.println("Никнейм:");
        String nickname = scanner.nextLine();
        System.out.println("Пароль:");
        String password = scanner.nextLine();

        AuthorizationUtil.putDataInRequestHeader(service, authorService, nickname, password);

       // authorService.addAuthor("first", "second", "last", "1997-06-05");
         // authorService.deleteAuthor(36);
       //  authorService.getAuthors().getItem().forEach(System.out::println);

    }
}
