
package com.epam.clients.soap.wsimport;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebResult;
import javax.jws.WebService;
import javax.jws.soap.SOAPBinding;
import javax.xml.bind.annotation.XmlSeeAlso;
import javax.xml.ws.Action;
import javax.xml.ws.FaultAction;


/**
 * This class was generated by the JAX-WS RI.
 * JAX-WS RI 2.2.9-b130926.1035
 * Generated source version: 2.2
 */
@WebService(name = "BookService", targetNamespace = "http://interfaces.soap.epam.com/")
@SOAPBinding(style = SOAPBinding.Style.RPC)
@XmlSeeAlso({
        ObjectFactory.class
})
public interface BookService {


    /**
     * @return returns com.epam.soap.interfaces.BookArray
     * @throws DataException
     */
    @WebMethod
    @WebResult(partName = "return")
    @Action(input = "http://interfaces.soap.epam.com/BookService/getAllRequest", output = "http://interfaces.soap.epam.com/BookService/getAllResponse", fault = {
            @FaultAction(className = DataException.class, value = "http://interfaces.soap.epam.com/BookService/getAll/Fault/DataException")
    })
    public BookArray getAll()
            throws DataException
    ;

    /**
     * @param arg3
     * @param arg2
     * @param arg5
     * @param arg4
     * @param arg1
     * @param arg0
     * @return returns com.epam.soap.interfaces.ResponseWrapper
     */
    @WebMethod
    @WebResult(partName = "return")
    @Action(input = "http://interfaces.soap.epam.com/BookService/addBookRequest", output = "http://interfaces.soap.epam.com/BookService/addBookResponse")
    public ResponseWrapper addBook(
            @WebParam(name = "arg0", partName = "arg0")
                    String arg0,
            @WebParam(name = "arg1", partName = "arg1")
                    int arg1,
            @WebParam(name = "arg2", partName = "arg2")
                    int arg2,
            @WebParam(name = "arg3", partName = "arg3")
                    String arg3,
            @WebParam(name = "arg4", partName = "arg4")
                    String arg4,
            @WebParam(name = "arg5", partName = "arg5")
                    long arg5);

    /**
     * @param arg0
     * @return returns com.epam.soap.interfaces.ResponseWrapper
     */
    @WebMethod
    @WebResult(partName = "return")
    @Action(input = "http://interfaces.soap.epam.com/BookService/deleteBookRequest", output = "http://interfaces.soap.epam.com/BookService/deleteBookResponse")
    public ResponseWrapper deleteBook(
            @WebParam(name = "arg0", partName = "arg0")
                    long arg0);

}
