
package com.epam.clients.soap.wsimport;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for Bookmark complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="Bookmark">
 *   &lt;complexContent>
 *     &lt;extension base="{http://interfaces.soap.epam.com/}entity">
 *       &lt;sequence>
 *       &lt;/sequence>
 *       &lt;attribute name="bookmarkId" use="required" type="{http://www.w3.org/2001/XMLSchema}long" />
 *       &lt;attribute name="userId" use="required" type="{http://www.w3.org/2001/XMLSchema}long" />
 *       &lt;attribute name="bookId" use="required" type="{http://www.w3.org/2001/XMLSchema}long" />
 *       &lt;attribute name="pageNumber" use="required" type="{http://www.w3.org/2001/XMLSchema}int" />
 *     &lt;/extension>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "Bookmark")
public class Bookmark
    extends Entity
{

    @XmlAttribute(name = "bookmarkId", required = true)
    protected long bookmarkId;
    @XmlAttribute(name = "userId", required = true)
    protected long userId;
    @XmlAttribute(name = "bookId", required = true)
    protected long bookId;
    @XmlAttribute(name = "pageNumber", required = true)
    protected int pageNumber;

    /**
     * Gets the value of the bookmarkId property.
     * 
     */
    public long getBookmarkId() {
        return bookmarkId;
    }

    /**
     * Sets the value of the bookmarkId property.
     * 
     */
    public void setBookmarkId(long value) {
        this.bookmarkId = value;
    }

    /**
     * Gets the value of the userId property.
     * 
     */
    public long getUserId() {
        return userId;
    }

    /**
     * Sets the value of the userId property.
     * 
     */
    public void setUserId(long value) {
        this.userId = value;
    }

    /**
     * Gets the value of the bookId property.
     * 
     */
    public long getBookId() {
        return bookId;
    }

    /**
     * Sets the value of the bookId property.
     * 
     */
    public void setBookId(long value) {
        this.bookId = value;
    }

    /**
     * Gets the value of the pageNumber property.
     * 
     */
    public int getPageNumber() {
        return pageNumber;
    }

    /**
     * Sets the value of the pageNumber property.
     * 
     */
    public void setPageNumber(int value) {
        this.pageNumber = value;
    }

    @Override
    public String toString() {
        return "Bookmark{" +
                "bookmarkId=" + bookmarkId +
                ", userId=" + userId +
                ", bookId=" + bookId +
                ", pageNumber=" + pageNumber +
                '}';
    }
}
