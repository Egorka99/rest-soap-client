
package com.epam.clients.soap.wsimport;

import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlEnumValue;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for EnumRole.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * <p>
 * <pre>
 * &lt;simpleType name="EnumRole">
 *   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *     &lt;enumeration value="User"/>
 *     &lt;enumeration value="Admin"/>
 *   &lt;/restriction>
 * &lt;/simpleType>
 * </pre>
 * 
 */
@XmlType(name = "EnumRole")
@XmlEnum
public enum EnumRole {

    @XmlEnumValue("User")
    USER("User"),
    @XmlEnumValue("Admin")
    ADMIN("Admin");
    private final String value;

    EnumRole(String v) {
        value = v;
    }

    public String value() {
        return value;
    }

    public static EnumRole fromValue(String v) {
        for (EnumRole c: EnumRole.values()) {
            if (c.value.equals(v)) {
                return c;
            }
        }
        throw new IllegalArgumentException(v);
    }

}
