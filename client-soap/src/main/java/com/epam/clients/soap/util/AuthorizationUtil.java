package com.epam.clients.soap.util;

import javax.xml.bind.annotation.*;
import javax.xml.ws.BindingProvider;
import javax.xml.ws.Service;
import javax.xml.ws.handler.MessageContext;
import java.util.*;

@XmlRootElement
public class AuthorizationUtil {

    public static void putDataInRequestHeader(Service service, Object serviceEndpointInterface, String login, String password) {
        Map<String, Object> requestContext = ((BindingProvider) serviceEndpointInterface).getRequestContext();
        requestContext.put(BindingProvider.ENDPOINT_ADDRESS_PROPERTY, service.getWSDLDocumentLocation().toString());

        Map<String, List<String>> headers = new HashMap<>();

        headers.put("Username", Collections.singletonList(Base64.getEncoder().encodeToString(login.getBytes())));
        headers.put("Password", Collections.singletonList(Base64.getEncoder().encodeToString(password.getBytes())));

        requestContext.put(MessageContext.HTTP_REQUEST_HEADERS, headers);
    }


}
