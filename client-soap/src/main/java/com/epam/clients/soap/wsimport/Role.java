
package com.epam.clients.soap.wsimport;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for role complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="role">
 *   &lt;complexContent>
 *     &lt;extension base="{http://interfaces.soap.epam.com/}entity">
 *       &lt;sequence>
 *       &lt;/sequence>
 *       &lt;attribute name="roleId" use="required" type="{http://www.w3.org/2001/XMLSchema}int" />
 *       &lt;attribute name="type" type="{http://interfaces.soap.epam.com/}EnumRole" />
 *     &lt;/extension>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "role")
public class Role
    extends Entity
{

    @XmlAttribute(name = "roleId", required = true)
    protected int roleId;
    @XmlAttribute(name = "type")
    protected EnumRole type;

    /**
     * Gets the value of the roleId property.
     * 
     */
    public int getRoleId() {
        return roleId;
    }

    /**
     * Sets the value of the roleId property.
     * 
     */
    public void setRoleId(int value) {
        this.roleId = value;
    }

    /**
     * Gets the value of the type property.
     * 
     * @return
     *     possible object is
     *     {@link EnumRole }
     *     
     */
    public EnumRole getType() {
        return type;
    }

    /**
     * Sets the value of the type property.
     * 
     * @param value
     *     allowed object is
     *     {@link EnumRole }
     *     
     */
    public void setType(EnumRole value) {
        this.type = value;
    }

    @Override
    public String toString() {
        return "Role{" +
                "roleId=" + roleId +
                ", type=" + type +
                '}';
    }
}
